
# This is the server logic for a Shiny web application.
# You can find out more about building applications with Shiny here:
#
# http://shiny.rstudio.com
#

library(shiny)
library(leaflet)

d<<-read.csv("saved_points.csv")

shinyServer(function(input, output) {

  points <- eventReactive(input$recalc, {
    txt<-input$page
    txt<-sprintf("<a href='%s'>Webpage</a>",txt)
    lat<-input$lat
    long<-input$long
    dd<-data.frame(lat,long,wb=txt)
    d<<-rbind(d,dd)
    write.csv(d,"saved_points.csv",row.names = FALSE)
    d
  }, ignoreNULL = FALSE)
  
  
  clr<-eventReactive(input$clear, {
    txt<-input$page
    txt<-sprintf("<a href='%s'>Webpage</a>",txt)
    lat<-input$lat
    long<-input$long
    dd<-data.frame(lat,long,wb=txt)
    d<<-dd
  }, ignoreNULL = FALSE)
  
  ld <- eventReactive(input$load, {
   d<<-read.csv("saved_points.csv")
   d
  }, ignoreNULL = FALSE)
  
  
  content <- eventReactive(input$recalc, {
    txt<-input$page
    sprintf("<a href='%s'>Webpage</a>",txt)
  }, ignoreNULL = FALSE)
  
  output$downloadData <- downloadHandler(
    filename = function()"Download.csv",
    content = function(file) {
      write.csv(d, file)
    })
  
  output$mymap <- renderLeaflet({
    
    a<-points()
    a<-clr()
    a<-ld()
    leaflet(d) %>%
      setView(lat = 50.742, lng = -1.89, zoom = 10) %>%
      addTiles(group="OSM") %>%
      addMiniMap(position = "bottomleft") %>%
      addScaleBar() %>%
      addGraticule(interval=1,group="Graticule") %>%
      addMeasure(primaryLengthUnit ='meters', secondaryLengthUnit='kilometers',primaryAreaUnit='hectares', secondaryAreaUnit='acres') %>%
      addProviderTiles(providers$Stamen.Toner, group = "Toner") %>%
      addProviderTiles("Esri.WorldImagery", group = "Satellite") %>%
      addProviderTiles("Esri.WorldShadedRelief", group = "Shaded relief",options = providerTileOptions(opacity =1))%>%
      addLayersControl(
        baseGroups = c("OSM", "Toner","Satellite","Shaded relief"),
        overlayGroups = c("Marker","Graticule"),
        options = layersControlOptions(collapsed = TRUE)) %>%
      
      addMarkers(group="Marker",popup=content()) 

  })
  
  observeEvent(input$mymap_click, {
    ## Get the click info like had been doing
    click <- input$mymap_click
    clat <- click$lat
    clng <- click$lng
    txt<-sprintf("Latitude=%s Longitude=%s \n Webpage=%s",clat,clng,content())
    dd<-data.frame(lat=clat,long=clng,wb=txt)
    d<<-rbind(d,dd)
    write.csv(d,"saved_points.csv",row.names = FALSE)
    leafletProxy('mymap') %>% # use the proxy to save computation
      addMarkers(lng=clng, lat=clat,popup=txt)
  })

})
